
## Getting Started in Searchable List

Clone the repository 
`git clone https://bobbygerez@bitbucket.org/bobbygerez/searchable-backend.git`

- Navigate to the cloned folder `cd searchable-backend`
- Install dependencies `composer install`
- Copy `.env.example` and create `.env`
- Create `test` database
- Import `searchable_list.sql` to `test` database
- Run the server `php artisan serve`
- Check if laravel is running  `http://127.0.0.1:8000`

