<?php 

namespace App\Repositories;

interface BaseInterface {

	public function all();
	public function filters($filters);
	public function get();
	public function limit($limit);
	public function paginate($limit);
	public function columns( $columns );
	public function with( $with );

	/**
	 * GLOBAL METHODS
	 */
	public function pregSplit(string $pattern, $value);

}