<?php

namespace App\Repositories;


class BaseRepository implements BaseInterface
{
    protected $model;
    
    public function all(){
        $this->model = $this->model->all();
        return $this;
    }

    public function filters($filters)
    {

        $filters =  $this->pregSplit('@;@', $filters);
        foreach($filters as $filter ){
           [$key, $value] = $this->pregSplit('@:@', $filter);
            $this->model = $this->model->where($key, $value);
        }
        return $this;
    }

    public function get()
    {
       return $this->model->get();

    }

    public function limit($limit)
    {

       $this->model = $this->model->limit($limit);
       return $this;

    }

    public function paginate($limit)
    {
        return  $this->model->paginate($limit);
    }

    public function columns( $columns )
    {

       $this->model = $this->model->select( $this->pregSplit('@,@', $columns) );
       return $this;
    }

    public function with( $with )
    {
        if($with){
            $relationships = $this->pregSplit('@,@', $with);
            $with = [];
            foreach( $relationships as $relationship){

                $containsSemiColon = preg_match('/;/', $relationship);
                //Remove string ';' and the succeeding character
                $forRelationship = strstr($relationship, ';', true);
                if ( $containsSemiColon ) {
                    //Remove string before ';' and remove the ';' also
                    [$key, $value ] = $this->pregSplit('@:@', str_replace( ';', '', strstr($relationship, ';') ) ) ;
                    $this->model = $this->model->with(array( $forRelationship => function($query) use ($key, $value) {
                        $query->where($key, $value);
                    }));
           
                }else{
                    $this->model = $this->model->with($relationship);
                }
            }
        }
       return $this;

    }

    /***
     * REUSABLE METHODS
     */
    public function pregSplit(string $pattern, $value){
        return preg_split($pattern, $value, NULL, PREG_SPLIT_NO_EMPTY);
    }
}
