<?php 

namespace App\Repositories\Product;

use App\Repositories\BaseInterface;

interface ProductInterface extends BaseInterface{

    public function constraintMin($min);
    public function constraintMax($max);
}