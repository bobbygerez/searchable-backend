<?php

namespace App\Repositories\Product;

use App\Repositories\BaseRepository;
use App\Models\Product;

class ProductRepository extends BaseRepository implements ProductInterface
{

    
    
    public function __construct()
    {
        $this->model = new Product;
    }

    public function constraintMin($min){

        $this->model = $this->model->when($min, function($q) use ($min) {
            $q->where('price', '>=', $min);
        });

        return $this;
        
    }

    public function constraintMax($max){

        $this->model = $this->model->when($max, function($q) use ($max) {
            $q->where('price', '<=', $max);
        });
        return $this;
    }

}
